package com.yelp.reviews.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.yelp.reviews.model.ReviewsResult;
import com.yelp.reviews.model.SearchResult;
import com.yelp.reviews.model.yelp.Business;
import com.yelp.reviews.service.SearchService;

@RequestMapping("/api/v1")
@RestController
public class SearchController {

	@Autowired
	private SearchService searchService;

	@GetMapping("/search")
	public ResponseEntity<Object> search(
			@RequestParam("location") String location, 
			@RequestParam("term") String term,
			@RequestParam(defaultValue = "10", required = false, name = "limit") String limit) {

		SearchResult result = searchService.searchBusiness(location, term, limit);

		return ResponseEntity.ok(result);
	}

	@GetMapping("/reviews/{id}")
	public ResponseEntity<Object> getReviews(@PathVariable("id") String id) {

		ReviewsResult result = searchService.getReviews(id);

		return ResponseEntity.ok(result);
	}

	@GetMapping("/details/{id}")
	public ResponseEntity<Object> getDetails(@PathVariable("id") String id) {

		Business details = searchService.getDetails(id);

		return ResponseEntity.ok(details);
	}
}
