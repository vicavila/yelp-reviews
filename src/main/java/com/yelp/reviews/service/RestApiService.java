package com.yelp.reviews.service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.yelp.reviews.exception.BadRequestException;

@Service
public class RestApiService {

	private static HttpClient client = HttpClient.newHttpClient();

	public byte[] get(Map<String, Object> params) {

		try {

			var request = HttpRequest.newBuilder()
					.uri(new URI(params.get("url").toString()))
					.header("Authorization", "Bearer " + params.get("apiKey").toString())
					.GET().build();

			var response = client.send(request, HttpResponse.BodyHandlers.ofByteArray());

			if (response.statusCode() != 200) {
				
				String str = new String(response.body());
				JsonObject convertedObject = new Gson().fromJson(str, JsonObject.class);
				String msg = convertedObject.get("error").getAsJsonObject().get("description").getAsString();
				throw new BadRequestException(msg);
			}

			return response.body();

		} catch (URISyntaxException | IOException | InterruptedException e) {
			e.printStackTrace();
			throw new BadRequestException("Error, yelp api cannot be reach.");
		}

	}
}
