package com.yelp.reviews.service;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.google.cloud.vision.v1.AnnotateImageRequest;
import com.google.cloud.vision.v1.AnnotateImageResponse;
import com.google.cloud.vision.v1.BatchAnnotateImagesResponse;
import com.google.cloud.vision.v1.FaceAnnotation;
import com.google.cloud.vision.v1.Feature;
import com.google.cloud.vision.v1.Image;
import com.google.cloud.vision.v1.ImageAnnotatorClient;
import com.google.protobuf.ByteString;
import com.yelp.reviews.exception.BadRequestException;
import com.yelp.reviews.model.CloudVissionResult;

@Service
public class GoogleCloudVissionService {

	public CloudVissionResult detectFacesGcs(String imageUrl) {

		if (imageUrl == null || imageUrl.isEmpty())
			return null;

		List<AnnotateImageRequest> requests = new ArrayList<>();

		ByteString imgBytes = null;
		try {
			imgBytes = ByteString.readFrom(new URL(imageUrl).openStream());
		} catch (IOException e1) {
			e1.printStackTrace();
			throw new BadRequestException("Error, invalid image url. " + imageUrl);
		}

		Image img = Image.newBuilder().setContent(imgBytes).build();
		Feature feat = Feature.newBuilder().setType(Feature.Type.FACE_DETECTION).build();

		AnnotateImageRequest request = AnnotateImageRequest.newBuilder().addFeatures(feat).setImage(img).build();
		requests.add(request);

		try (ImageAnnotatorClient client = ImageAnnotatorClient.create()) {

			BatchAnnotateImagesResponse response = client.batchAnnotateImages(requests);
			List<AnnotateImageResponse> responses = response.getResponsesList();

			for (AnnotateImageResponse res : responses) {
				if (res.hasError()) {
					throw new BadRequestException(String.format("Error: %s%n", res.getError().getMessage()));
				}

				// For full list of available annotations, see http://g.co/cloud/vision/docs
				CloudVissionResult result = new CloudVissionResult();
				for (FaceAnnotation annotation : res.getFaceAnnotationsList()) {

					result.setJoyLikelihood(annotation.getJoyLikelihood().toString());
					result.setSorrowLikelihood(annotation.getSorrowLikelihood().toString());
				}
				return result;

			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new BadRequestException("Error, Google Cloud Vission cannot be reach.");
		}
		return null;
	}
}
