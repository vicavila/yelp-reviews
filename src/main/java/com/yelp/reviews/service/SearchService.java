package com.yelp.reviews.service;

import com.yelp.reviews.model.ReviewsResult;
import com.yelp.reviews.model.SearchResult;
import com.yelp.reviews.model.yelp.Business;

public interface SearchService {

	SearchResult searchBusiness(String location, String term, String limit);

	Business getDetails(String id);

	ReviewsResult getReviews(String id);

}
