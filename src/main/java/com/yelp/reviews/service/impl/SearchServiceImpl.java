package com.yelp.reviews.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yelp.reviews.exception.BadRequestException;
import com.yelp.reviews.model.CloudVissionResult;
import com.yelp.reviews.model.ReviewItem;
import com.yelp.reviews.model.ReviewsResult;
import com.yelp.reviews.model.SearchItem;
import com.yelp.reviews.model.SearchResult;
import com.yelp.reviews.model.yelp.Business;
import com.yelp.reviews.model.yelp.Review;
import com.yelp.reviews.model.yelp.YelpReviewsAPIResponse;
import com.yelp.reviews.model.yelp.YelpSearchAPIResponse;
import com.yelp.reviews.service.GoogleCloudVissionService;
import com.yelp.reviews.service.RestApiService;
import com.yelp.reviews.service.SearchService;

@Service
public class SearchServiceImpl implements SearchService {

	@Autowired
	private RestApiService apiService;

	@Autowired
	private GoogleCloudVissionService cloudVissionService;

	private static final String YELP_BUSINESS_API = "https://api.yelp.com/v3/businesses";
	private static final String YELP_API_KEY = "EfJxKf_LhOj9oBdIfYm1yxYRpLZrJmCRMUgJSMszN9hF8MUG5-toZhMBafmja0zhO-PAXOUAR7eP2KUQ1F3UxDk5XRLWWv11Hk_LZOxhMXvKTdGSw-dIvNAc55CeYHYx";

	private static final String REVIEWS_URL = "http://localhost:8080/api/v1/reviews/";
	private static final String DETAILS_URL = "http://localhost:8080/api/v1/details/";

	private static YelpSearchAPIResponse businessSearchResult = new YelpSearchAPIResponse();

	@Override
	public SearchResult searchBusiness(String location, String term, String limit) {

		String url = String.format("%s/search?%s=%s&%s=%s&%s=%s", 
				YELP_BUSINESS_API, 
				"location", location, 
				"term", term,
				"limit", limit);

		Map<String, Object> params = new HashMap<>();
		params.put("url", url);
		params.put("apiKey", YELP_API_KEY);

		byte[] byteResponse = apiService.get(params);

		try {
			
			businessSearchResult = new ObjectMapper()
					.readValue(byteResponse, YelpSearchAPIResponse.class);
			
		} catch (IOException e) {
			e.printStackTrace(); // proper way is to log this
			throw new BadRequestException("Error parsing yelp search api response.");
		}

		return parseSearchResult(businessSearchResult);

	}

	@Override
	public ReviewsResult getReviews(String id) {

		String url = String.format("%s/%s/reviews", YELP_BUSINESS_API, id);

		Map<String, Object> params = new HashMap<>();
		params.put("url", url);
		params.put("apiKey", YELP_API_KEY);

		byte[] byteResponse = apiService.get(params);
		YelpReviewsAPIResponse reviews = null;

		try {
			reviews = new ObjectMapper().readValue(byteResponse, YelpReviewsAPIResponse.class);

		} catch (IOException e) {
			e.printStackTrace(); // proper way is to log this
			throw new BadRequestException("Error parsing yelp reviews api response.");
		}

		return parseReviews(reviews);
	}

	@Override
	public Business getDetails(String id) {

		for (Business b : businessSearchResult.getBusinesses()) {

			if (b.getId().equals(id))
				return b;
		}
		throw new BadRequestException("Business details not found for Id: " + id);
	}

	private ReviewsResult parseReviews(YelpReviewsAPIResponse reviews) {

		ReviewsResult result = new ReviewsResult();
		List<ReviewItem> reviewList = new ArrayList<>();
		for (Review r : reviews.getReviews()) {

			ReviewItem item = new ReviewItem();
			item.setRating(r.getRating());
			item.setUser_name(r.getUser().getName());
			item.setProfile_url(r.getUser().getProfile_url());

			String imageUrl = r.getUser().getImage_url();
			item.setImage_url(imageUrl);
			item.setReview(r.getText());
			item.setCreated(r.getTime_created());

			CloudVissionResult cloudVission = cloudVissionService.detectFacesGcs(imageUrl);
			if (cloudVission != null) {
				item.setCloudVission(cloudVission);
			}
			reviewList.add(item);

		}
		result.setReviews(reviewList);

		return result;
	}

	private SearchResult parseSearchResult(YelpSearchAPIResponse response) {

		SearchResult result = new SearchResult();
		List<SearchItem> items = new ArrayList<>();

		for (Business b : response.getBusinesses()) {

			SearchItem item = new SearchItem();
			item.setName(b.getName());
			item.setImage_url(b.getImage_url());
			item.setPhone(b.getDisplay_phone());
			item.setAddress(String.join(", ", b.getLocation().getDisplay_address()));
			item.setClosed(b.isIs_closed());
			item.setRating(b.getRating());
			item.setReviews_url(REVIEWS_URL + b.getId());
			item.setDetails_url(DETAILS_URL + b.getId());

			String categories = b.getCategories()
					.stream().map(c -> c.getTitle())
					.collect(Collectors.joining(", "));

			item.setCategory(categories);
			item.setWebsite_url(b.getUrl());

			items.add(item);
		}
		result.setSearchItems(items);
		result.setTotal(response.getTotal());

		return result;
	}

}
