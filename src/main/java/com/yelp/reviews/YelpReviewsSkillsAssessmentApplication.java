package com.yelp.reviews;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YelpReviewsSkillsAssessmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(YelpReviewsSkillsAssessmentApplication.class, args);
	}

}
