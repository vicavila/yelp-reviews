package com.yelp.reviews.model;

public class CloudVissionResult {

	private String joyLikelihood;
	private String sorrowLikelihood;

	public String getJoyLikelihood() {
		return joyLikelihood;
	}

	public void setJoyLikelihood(String joyLikelihood) {
		this.joyLikelihood = joyLikelihood;
	}

	public String getSorrowLikelihood() {
		return sorrowLikelihood;
	}

	public void setSorrowLikelihood(String sorrowLikelihood) {
		this.sorrowLikelihood = sorrowLikelihood;
	}

}
