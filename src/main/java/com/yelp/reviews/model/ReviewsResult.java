package com.yelp.reviews.model;

import java.util.List;

public class ReviewsResult {

	private List<ReviewItem> reviews;

	public List<ReviewItem> getReviews() {
		return reviews;
	}

	public void setReviews(List<ReviewItem> reviews) {
		this.reviews = reviews;
	}

}
