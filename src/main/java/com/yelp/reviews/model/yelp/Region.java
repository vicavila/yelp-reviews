package com.yelp.reviews.model.yelp;

public class Region {

	private Coordinates center;

	public Coordinates getCenter() {
		return center;
	}

	public void setCenter(Coordinates center) {
		this.center = center;
	}
	
	
}
