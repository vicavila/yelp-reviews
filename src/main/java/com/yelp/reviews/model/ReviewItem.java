package com.yelp.reviews.model;

public class ReviewItem {

	private double rating;
	private String user_name;
	private String profile_url;
	private String image_url;
	private String review;
	private String created;
	private CloudVissionResult cloudVission;

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getProfile_url() {
		return profile_url;
	}

	public void setProfile_url(String profile_url) {
		this.profile_url = profile_url;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public String getReview() {
		return review;
	}

	public void setReview(String review) {
		this.review = review;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public CloudVissionResult getCloudVission() {
		return cloudVission;
	}

	public void setCloudVission(CloudVissionResult cloudVission) {
		this.cloudVission = cloudVission;
	}

}
