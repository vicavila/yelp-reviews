package com.yelp.reviews.model;

import java.util.List;

public class SearchResult {

	private List<SearchItem> searchItems;
	private int total;

	public List<SearchItem> getSearchItems() {
		return searchItems;
	}

	public void setSearchItems(List<SearchItem> searchItems) {
		this.searchItems = searchItems;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

}
